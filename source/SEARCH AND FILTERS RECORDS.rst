SEARCH AND FILTERS RECORDS
==========================
The user can easily search for a specific record by typing any search parameter in the search box. For example, to search for an indicator entry, type any text string such as indicator code or name in the search box as shown in Figure 19. For more narrowed search space, you can filter the rows using the filter options provided in the filter panel shown on the right.  

.. image:: search.png
	:align: center
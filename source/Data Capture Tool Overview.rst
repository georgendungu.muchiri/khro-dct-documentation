Data Capture Tool Overview 
==========================

The KHRO Data Capture Tool is a Python-based App developed mainly using Django Web and API Frameworks. The App has navigable menus, forms, RESTful API, and action buttons that simplify online data entry for indicators, data elements, research, health commodities and other health-related resources. 
The landing page (dashboard) has several features that makes it possible to type, import, and export and search for data. Once data is captured and stored in the App’s database, Talend, an open source Java-based data integration tool is used to migrate the data into the Staging Database depicted in the architectural model.  


Dashboard
*********

Once logged into the Admin page, first page displayed such as shown in Figure 2 is called a dashboard. The DCT dashboard is a central interface that give you access to quick menus available for data entry, import and export. The dashboard is more convenient when using portable devices like tablets and mobile phones. The dashboard loaded depends on the privileges you have been assigned to by the system administrator. For example, the dashboard shown in Figure 4 that gives a user called Laura access to Indicators, Elements and Sources.

.. image:: dashboard.png
	:align: center
 
*Figure 2*


The dashboard has more menus that you can access from DCT. For example, Figure 2 shows that the Admin user has access to Home, Health Indicators, Research, Data Elements, Regions, Commodities, Sources Data_Wizard and Authentication menus. Note that on the right of the dashboard page is Recent actions panel that contains information on the add, delete or edit actions you have done in the App. The App has been implemented using modern responsive (context-aware) design principles that allows the same interface to dynamically scale to any device and execution platform. For example, Figure 3 (a) shows how the Admin dashboard appears on a smartphone or mobile phone while Figure 3 (b) shows how indicator data entry form appears on the phone.

.. image:: mobile_dashboard.png
	:align: center

*Figure 3*

KHRO-DCT Benefits
*****************
The following are some of the benefits that will be realized by adopting DCT as an alternative data capture and submission tool;

#.	Member countries will be able easily upload data from their systems or electronic templates hence resulting to reduced delays and non-compliance to data submission timelines.
#.	The Ministry will be able to meet satisfaction of the member countries through improved service delivery as has been demonstrated during development of the KHRO platform. 
#.	There be improved morale and enthusiasm to submit data to the regional office due to reduced stress and fatigue experienced in the manual-based data collection methods.
#.	The online data submission method has the potential to improved internal decision making processes due to availability of real-time and reliable reports from KHRO web portal.
#.	Once data is received from the DCT, KHRO has implemented validation and de-duplication mechanisms that make is possible to compare data from several sources. For example, using different Talend and database stored subroutines (triggers and procedures), it is possible to identify and flag duplicate data entry for the same indicator from the same or different sources. 


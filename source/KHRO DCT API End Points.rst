KHRO DCT API End Points
=======================

In distributed web services, the acronym REST stands for REpresentational State Transfer.  Thus, a RESTful web application exposes information from a database server to a to a web client such as KHRO without revealing the structural and connection details at the back-end. Once the response is received in JSON, XML, YAML or any other REST supported format, the client such as KHRO can then create new instances such as new indicator.
To take care of REST, the Data Capture Tool has implemented Open APIs that provides authenticated uses ability to request and load data using all the HTTP methods (POST, GET, PUT, PATCH and DELETE).  We use POST to create that resource; GET to retrieve a resource; PUT to update a resource; and DELETE to remove any resource. Since POST, PUT, PATCH and DELETE requests alter the database, admin-privileged user has to authenticate before using these methods. to avoid making changes or deleting critical metadata and records PATCH, PUT and DELETE privileges are only granted to superusers (Figure 22). Authentication methods supported include Basic (username and password), and secret token method that will be used as the primary mechanism for connecting third-party clients to the Data Capture Tool. 

.. image:: API_End_Point.png
	:align: center
* *Figure 22*

The KHRO Data Capture Tool API architecture has been modelled and implemented using Django REST framework due to its flexibility in building Web APIs. By default, Django REST supports JSON (JavaScript Object Notation) as a format for mechanism for consuming API endpoints. JSON is an open standard data format that is lightweight and human-readable, and looks like Objects do in JavaScript. To consume data in JSON format, the users has to use the resource links such as khro.xyz/api/stg_location/ shown in swagger documentation of Figure 23 (https://khro.xyz/swagger-docs/).  

.. image:: endpoints_doc.png
	:align: center

* *Figure 23*
Figure 24 shows JSON response produced after hitting https://khro.xyz/stg_locations/ endpoint. The API are also accessible using utilities such cURL and Postman that support API request-response mechanisms.

.. image:: locations.png
	:align: center

* *Figure 24*
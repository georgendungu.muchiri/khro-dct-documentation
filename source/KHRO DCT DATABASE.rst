KHRO DCT DATABASE
=================
The Data Capture Tool uses Object Relational Model supported by Django to generate a physical schema in MySQL. The generated schema is then fine-tuned using MySQL Workbench and other database design tools to improve its performance and data integrity. The data dictionary and design sheets used to implement the data model were shared with the technical review experts mainly from University of Nairobi, iLab (Strathmore University); AFRO-Brazzaville; Mr. Daniel Mbugua (software engineer and Django Expert-Savannah Informatics) and the WHO Programme lead-Mr. Cosmas Leonard.  
Figure 25 shows the current physical (implemented) database schema. The schema has a total of 59 tables, triggers and views that are used to manage validation and storage of data received from multiple used is a distributed online environment.

.. image:: database.png
	:align: center


KHRO DCT Modules
================

The data capture tool has six modules but the dashboard loaded depends on user privileges.  For privileged users like System Admins, the current prototype has nine (9) modules shown in Figure 4. These are Home, Indicators, Research, Elements, Regions, Commodities, Authentication, Sources and Data_Wizard. Each of these modules has menus and forms used for data entry or export. The current version mainly supports data entry through forms and import wizard that takes the user though a step-by-step process of mapping the template columns to the DCT database tables (schema).


.. image:: DCT_Module.png
	:align: center

* *Figure 4*

Loading KHRO-DCT
****************
The data capture tool prototype hosted on temporarily digital ocean servers provides the user with a dashboard that makes the process of capturing, processing and exporting data easier. To load the dashboard, proceed as follows:

#.	On the desktop, point to and then click a web browser of your choice (Chrome, Mozilla Firefox, Windows Explorer). 

#.	On the browser’s address bar, type: khro.xyz. The login page shown in the Figure 5 below is displayed.

#.	Enter username (your registered email) and password provided by the system administrator. The figure above shows login entry for the current programme manager:  cosmasl@who.int but the password is concealed for privacy reason. Once you enter the credentials, click the blue Login button. 


.. image:: dct_login.png
	:align: center

* *Figure 5*



Dashboard and Modules 
*********************
The data capture tool has several modules that makes it possible to capture data, import, and export data. These includes the dashboard, modules, search boxes, filter panels and action buttons. Once you launch the application, first page displayed is the Dashboard. In computer terms, a dashboard is a user interface often displayed on a web page which is linked to a database that allows the user to enter data or generate summary reports. This makes it more convenient when using devices with small screen size like mobile phones. If you are not an Admin user, the dashboard loaded will depend on the rights and privileges assigned. For example, Laura’s dashboard shown in Figure 6 that gives her access to only 3 modules namely Indicators, Elements and Sources.
Note that on the right of the dashboard page is Recent actions panel that contains information on the add, delete or edit actions you have done in the App. The following section discusses all the modules implemented in the App.

.. image:: restricted.png
	:align: center

* *Figure 6*

The Admin portal has the main dashboard and all modules including Authentication used to add new users as shown in Figure 7. 

.. image:: superadmin.png
	:align: center

* *Figure 7*

Home Module
***********
Next to the dashboard is the Home module used to perform most of the primary tasks adding, deleting or editing indicator domains, disaggregations (modalities), data types, and data sources. Figure 8 shows the Home module for Admin user with seven menus used to make changes to indicator domains and related metadata.

.. image:: home_module.png
	:align: center

* *Figure 8*

Indicators Module
*****************
The Indicators module is central to this application because it is used to add and process indicator data. The Indicators module for Admin users (Figure 9) has six menus used to add, delete or modify indicator records. The module also allows the user view archived indicator data as well as import and export indicator data in CVS or Excel formats.

.. image:: indicators.png
	:align: center

* *Figure 9*

Research Module
****************

Research module is used to support business process relating to research and knowledge management. The module shown in Figure 10 has eight (8) menus relating to Research Themes, Topics, ICD 11 Taxonomy, Ethics Research Approvals, and knowledge products that are sub-classes into proposals and publications.  The module is also used for capturing other resources such M&E toolkits, health policies, strategic plans, and policy briefs. The module also allows the user to import and export knowledge resources in CVS or Excel formats 

.. image:: research.png
	:align: center

* *Figure 10*
The module also allows the user to import and export knowledge resources in CVS or Excel formats as shown in Figure 11.

.. image:: fig11.png
	:align: center 

* *Figure 11*

Elements Module
****************

The Elements module is almost similar to the Indicators module but it is used to capture raw data from aggregate systems like DHIS2. This type of data mostly gives counts such as number of women who attended ANC 1 between January and August 2019. The Elements module for Admin users (Figure 12) has four menus used to add, delete or modify data elements metadata and records. The module also allows the user to import and export data elements in CSV and Excel formats.


.. image:: elements.png
	:align: center

* *Figure 12*
Regions Module
***************

The Regions module implements a hierarchical data structure that allows for the user to create regions such as counties, then create sub-counties under a county. This model replicates the Organisation Units (OrgUnit) model implemented in DHIS2. Figure 13 below shows that the module for Admin users has two menus used to add, delete or modify orgunit level details such county level, and as location e.g. Baringo County. The module also allows the user to export countries details in CSV or Excel formats.


.. image:: regions.png
	:align: center

* *Figure 13*

Commodities Module
******************

During the design of KHRO, one of the key data custodian that provided API endpoint on health commodities data was the Kenya Medical Supplies Agency (KEMSA), a state corporation under the Ministry of Health. KEMSA uses Logistic Management Information System (LMIS) to manage procurement and sale of drugs and other medical supplies However, because not all health commodities are procured through KEMSA the stakeholders deemed it necessary to provide a platform that captures health related commodity data from multiple sources.  The commodities module shown in Figure 14 was has been implemented based on the business logic shared by LMIS though various discussions and shared API endpoints. 


.. image:: commodities.png
	:align: center

* *Figure 14*

Authentication Module
*********************
The Settings module shown is only available to Super Admin users used to grant or revoke rights and privileges to users and groups. Figure 15 shows how to created and assign privileges to a group named KHRO Admin. Due to security and privacy reasons, this module DOES NOT allow the user’s credentials to be imported into the App through spreadsheet or API endpoints.

.. image:: khroauthentication.png
	:align: center

* *Figure 15*

Data Import Modules
*******************

The Source and Data_Wizard modules are purposely used to simplify data import from Excel and CSV templates. To import data from a template, choose the source location by clicking Add File Source to display the page shown in Figure 16.

.. image:: dataimport.png
	:align: center
* *Figure 16*

Once you select the file location, click save and the selection will appear on the source page.  You can then click the item in the list to initiate the wizard-driven import process. Once the import is complete. The success or failed import process is displayed on the Data_Wizard page of the Data_Wizard module as shown in Figure 17.

.. image:: datawizard.png
	:align: center 

* *Figure 17*


Admin Actions
*************
The add, delete, edit, import and export operations are achieved through clicking the relevant Action buttons. Depending on the privileges, your module interface may have the following buttons:

#.  Add: This a green button located on the top right used to add new records
#. 	Delete: This a red button placed on the bottom left used to delete an entry
#. 	Import button: This button is used to initiate import from CSV or Excel templates
#. 	Export button: This button is used to initiate export to CSV or Excel templates
#. 	Go: The Go button is used to execute an action selected from the Action or Format dropdown list such as shown in Figure 18:

.. image:: action_buttons.png
	:align: center

* *Figure 18*


��fR      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�
Deployment�h]�h	�Text����
Deployment�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�6C:\laragon\www\DCT_documentation\source\Deployment.rst�hKubh)��}�(hhh]�(h)��}�(h� Logging into the Dev environment�h]�h� Logging into the Dev environment�����}�(hh1hh/hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh,hhhh+hKubh	�	paragraph���)��}�(h�!**API Authentication (Passport)**�h]�h	�strong���)��}�(hhAh]�h�API Authentication (Passport)�����}�(hhhhEubah}�(h ]�h"]�h$]�h&]�h(]�uh*hChh?ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh,hhubh>)��}�(hX�  BLIS v3.0 API uses tokens to authenticate users and does not maintain session state between requests. It therefore uses Laravel Passport, which provides a full OAuth2 server implementation for the application in a matter of minutes. Passport is built on top of the `League OAuth2 server <https://github.com/thephpleague/oauth2-server>`_  that is maintained by Andy Millington and Simon Hamp. More on Laravel Passport `here <https://laravel.com/docs/5.6/passport>`_�h]�(hX	  BLIS v3.0 API uses tokens to authenticate users and does not maintain session state between requests. It therefore uses Laravel Passport, which provides a full OAuth2 server implementation for the application in a matter of minutes. Passport is built on top of the �����}�(hX	  BLIS v3.0 API uses tokens to authenticate users and does not maintain session state between requests. It therefore uses Laravel Passport, which provides a full OAuth2 server implementation for the application in a matter of minutes. Passport is built on top of the �hhXhhhNhNubh	�	reference���)��}�(h�G`League OAuth2 server <https://github.com/thephpleague/oauth2-server>`_�h]�h�League OAuth2 server�����}�(h�League OAuth2 server�hhcubah}�(h ]�h"]�h$]�h&]�h(]��name��League OAuth2 server��refuri��-https://github.com/thephpleague/oauth2-server�uh*hahhXubh	�target���)��}�(h�0 <https://github.com/thephpleague/oauth2-server>�h]�h}�(h ]��league-oauth2-server�ah"]�h$]��league oauth2 server�ah&]�h(]��refuri�huuh*hv�
referenced�KhhXubh�Q  that is maintained by Andy Millington and Simon Hamp. More on Laravel Passport �����}�(h�Q  that is maintained by Andy Millington and Simon Hamp. More on Laravel Passport �hhXhhhNhNubhb)��}�(h�/`here <https://laravel.com/docs/5.6/passport>`_�h]�h�here�����}�(h�here�hh�ubah}�(h ]�h"]�h$]�h&]�h(]��name�h�ht�%https://laravel.com/docs/5.6/passport�uh*hahhXubhw)��}�(h�( <https://laravel.com/docs/5.6/passport>�h]�h}�(h ]��here�ah"]�h$]��here�ah&]�h(]��refuri�h�uh*hvh�KhhXubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK	hh,hhubh>)��}�(h�**Installation**�h]�hD)��}�(hh�h]�h�Installation�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hChh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh,hhubh	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(h�[Install Passport via the Composer package manager: ::

  composer require laravel/passport
�h]�(h>)��}�(h�5Install Passport via the Composer package manager: ::�h]�h�2Install Passport via the Composer package manager:�����}�(h�2Install Passport via the Composer package manager:�hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh�ubh	�literal_block���)��}�(h�!composer require laravel/passport�h]�h�!composer require laravel/passport�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]��	xml:space��preserve�uh*h�hh+hKhh�ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(hX)  The Passport service provider registers its own database migration directory with the framework, so you should migrate your database after registering the provider. The Passport migrations will create the tables your application needs to store clients and access tokens: ::

  php artisan migrate
�h]�(h>)��}�(hX  The Passport service provider registers its own database migration directory with the framework, so you should migrate your database after registering the provider. The Passport migrations will create the tables your application needs to store clients and access tokens: ::�h]�hX  The Passport service provider registers its own database migration directory with the framework, so you should migrate your database after registering the provider. The Passport migrations will create the tables your application needs to store clients and access tokens:�����}�(hX  The Passport service provider registers its own database migration directory with the framework, so you should migrate your database after registering the provider. The Passport migrations will create the tables your application needs to store clients and access tokens:�hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh�ubh�)��}�(h�php artisan migrate�h]�h�php artisan migrate�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*h�hh+hKhh�ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(hX6  Next, you should run the ``passport:install`` command. This command will create the encryption keys needed to generate secure access tokens. In addition, the command will create "personal access" and "password grant" clients which will be used to generate access tokens: ::

      php artisan passport:install
�h]�(h>)��}�(hX  Next, you should run the ``passport:install`` command. This command will create the encryption keys needed to generate secure access tokens. In addition, the command will create "personal access" and "password grant" clients which will be used to generate access tokens: ::�h]�(h�Next, you should run the �����}�(h�Next, you should run the �hj#  ubh	�literal���)��}�(h�``passport:install``�h]�h�passport:install�����}�(hhhj.  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj#  ubh�� command. This command will create the encryption keys needed to generate secure access tokens. In addition, the command will create “personal access” and “password grant” clients which will be used to generate access tokens:�����}�(h�� command. This command will create the encryption keys needed to generate secure access tokens. In addition, the command will create "personal access" and "password grant" clients which will be used to generate access tokens:�hj#  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhj  ubh�)��}�(h�php artisan passport:install�h]�h�php artisan passport:install�����}�(hhhjG  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*h�hh+hKhj  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(hX  After running this command, add the ``Laravel\Passport\HasApiTokens`` trait to your ``App\User`` model. This trait will provide a few helper methods to your model which allow you to inspect the authenticated user's token and scopes: ::

      <?php

      namespace App;

      use Laravel\Passport\HasApiTokens;
      use Illuminate\Notifications\Notifiable;
      use Illuminate\Foundation\Auth\User as Authenticatable;

      class User extends Authenticatable
      {
          use HasApiTokens, Notifiable;
      }
�h]�(h>)��}�(h��After running this command, add the ``Laravel\Passport\HasApiTokens`` trait to your ``App\User`` model. This trait will provide a few helper methods to your model which allow you to inspect the authenticated user's token and scopes: ::�h]�(h�$After running this command, add the �����}�(h�$After running this command, add the �hj_  ubj-  )��}�(h�!``Laravel\Passport\HasApiTokens``�h]�h�Laravel\Passport\HasApiTokens�����}�(hhhjh  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj_  ubh� trait to your �����}�(h� trait to your �hj_  ubj-  )��}�(h�``App\User``�h]�h�App\User�����}�(hhhj{  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj_  ubh�� model. This trait will provide a few helper methods to your model which allow you to inspect the authenticated user’s token and scopes:�����}�(h�� model. This trait will provide a few helper methods to your model which allow you to inspect the authenticated user's token and scopes:�hj_  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhj[  ubh�)��}�(h��<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
}�h]�h��<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
}�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*h�hh+hKhj[  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(hX  Next, you should call the ``Passport: :routes`` method within the ``boot`` method of your AuthServiceProvider. This method will register the routes necessary to issue access tokens and revoke access tokens, clients, and personal access tokens: ::

      <?php

      namespace App\Providers;

      use Laravel\Passport\Passport;
      use Illuminate\Support\Facades\Gate;
      use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

      class AuthServiceProvider extends ServiceProvider
      {
          /**
           * The policy mappings for the application.
           *
           * @var array
           */
          protected $policies = [
              'App\Model' => 'App\Policies\ModelPolicy',
          ];

          /**
           * Register any authentication / authorization services.
           *
           * @return void
           */
          public function boot()
          {
              $this->registerPolicies();

              Passport::routes();
          }
      }
�h]�(h>)��}�(h��Next, you should call the ``Passport: :routes`` method within the ``boot`` method of your AuthServiceProvider. This method will register the routes necessary to issue access tokens and revoke access tokens, clients, and personal access tokens: ::�h]�(h�Next, you should call the �����}�(h�Next, you should call the �hj�  ubj-  )��}�(h�``Passport: :routes``�h]�h�Passport: :routes�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh� method within the �����}�(h� method within the �hj�  ubj-  )��}�(h�``boot``�h]�h�boot�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh�� method of your AuthServiceProvider. This method will register the routes necessary to issue access tokens and revoke access tokens, clients, and personal access tokens:�����}�(h�� method of your AuthServiceProvider. This method will register the routes necessary to issue access tokens and revoke access tokens, clients, and personal access tokens:�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK(hj�  ubh�)��}�(hXn  <?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}�h]�hXn  <?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*h�hh+hK*hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(hX�  Finally, in your ``config/auth.php`` configuration file, you should set the ``driver`` option of the ``api`` authentication guard to ``passport``. This will instruct your application to use Passport's ``TokenGuard`` when authenticating incoming API requests: ::

     'guards' => [
         'web' => [
             'driver' => 'session',
             'provider' => 'users',
         ],

         'api' => [
             'driver' => 'passport',
             'provider' => 'users',
         ],
     ],
�h]�(h>)��}�(hX  Finally, in your ``config/auth.php`` configuration file, you should set the ``driver`` option of the ``api`` authentication guard to ``passport``. This will instruct your application to use Passport's ``TokenGuard`` when authenticating incoming API requests: ::�h]�(h�Finally, in your �����}�(h�Finally, in your �hj�  ubj-  )��}�(h�``config/auth.php``�h]�h�config/auth.php�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh�( configuration file, you should set the �����}�(h�( configuration file, you should set the �hj�  ubj-  )��}�(h�
``driver``�h]�h�driver�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh� option of the �����}�(h� option of the �hj�  ubj-  )��}�(h�``api``�h]�h�api�����}�(hhhj(  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh� authentication guard to �����}�(h� authentication guard to �hj�  ubj-  )��}�(h�``passport``�h]�h�passport�����}�(hhhj;  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh�:. This will instruct your application to use Passport’s �����}�(h�8. This will instruct your application to use Passport's �hj�  ubj-  )��}�(h�``TokenGuard``�h]�h�
TokenGuard�����}�(hhhjN  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubh�+ when authenticating incoming API requests:�����}�(h�+ when authenticating incoming API requests:�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKJhj�  ubh�)��}�(h��'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'passport',
        'provider' => 'users',
    ],
],�h]�h��'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'passport',
        'provider' => 'users',
    ],
],�����}�(hhhjg  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*h�hh+hKLhj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]��enumtype��arabic��prefix�h�suffix��.�uh*h�hh,hhhh+hKubeh}�(h ]�� logging-into-the-dev-environment�ah"]�h$]�� logging into the dev environment�ah&]�h(]�uh*h
hhhhhh+hKubh)��}�(hhh]�(h)��}�(h�Seeders�h]�h�Seeders�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKYubh>)��}�(hX  The Dev Seeder adds dummy data to the database. This includes users, tests, specimen, e.t.c. An admin account is also created for test purposes. The admin has all access rights and can access all routes on BLIS. The database can be seeded using ``php artisan db:seed``�h]�(h��The Dev Seeder adds dummy data to the database. This includes users, tests, specimen, e.t.c. An admin account is also created for test purposes. The admin has all access rights and can access all routes on BLIS. The database can be seeded using �����}�(h��The Dev Seeder adds dummy data to the database. This includes users, tests, specimen, e.t.c. An admin account is also created for test purposes. The admin has all access rights and can access all routes on BLIS. The database can be seeded using �hj�  hhhNhNubj-  )��}�(h�``php artisan db:seed``�h]�h�php artisan db:seed�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j,  hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK[hj�  hhubeh}�(h ]��seeders�ah"]�h$]��seeders�ah&]�h(]�uh*h
hhhhhh+hKYubh)��}�(hhh]�(h)��}�(h�Logging into the Live System�h]�h�Logging into the Live System�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK^ubh>)��}�(h��The API has a familiar login screen to most systems. Use your assigned Email and Password combinations to gain access into the system. The figure below shows this screen.�h]�h��The API has a familiar login screen to most systems. Use your assigned Email and Password combinations to gain access into the system. The figure below shows this screen.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK`hj�  hhubh	�image���)��}�(h�3.. image:: images/login.PNG
        :align: center
�h]�h}�(h ]�h"]�h$]�h&]�h(]��align��center��uri��images/login.PNG��
candidates�}��*�j�  suh*j�  hj�  hhhh+hNubh>)��}�(h�AIf a wrong combination is used, you shall be notified by a popup.�h]�h�AIf a wrong combination is used, you shall be notified by a popup.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKehj�  hhubeh}�(h ]��logging-into-the-live-system�ah"]�h$]��logging into the live system�ah&]�h(]�uh*h
hhhhhh+hK^ubh)��}�(hhh]�(h)��}�(h�Main Dashboard (Admin)�h]�h�Main Dashboard (Admin)�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj  hhhh+hKhubh>)��}�(hX  Once Authenticated, Blis V3 provides a compact and easy to use Interface. The number of visible components depends on the Authenticated User's Role and access rights. The image below shows the administrator's view. The admin has access rights to all components.�h]�hX	  Once Authenticated, Blis V3 provides a compact and easy to use Interface. The number of visible components depends on the Authenticated User’s Role and access rights. The image below shows the administrator’s view. The admin has access rights to all components.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKjhj  hhubj�  )��}�(h�2.. image:: images/Home.PNG
        :align: center
�h]�h}�(h ]�h"]�h$]�h&]�h(]��align��center��uri��images/Home.PNG�j�  }�j�  j8  suh*j�  hj  hhhh+hNubh>)��}�(h�**Sidebar**�h]�hD)��}�(hj<  h]�h�Sidebar�����}�(hhhj>  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hChj:  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKohj  hhubh>)��}�(h�]The side bar provides easy access to the components and can be expanded to reveal more items.�h]�h�]The side bar provides easy access to the components and can be expanded to reveal more items.�����}�(hjS  hjQ  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKqhj  hhubj�  )��}�(h�5.. image:: images/sidebar.PNG
        :align: center
�h]�h}�(h ]�h"]�h$]�h&]�h(]��align��center��uri��images/sidebar.PNG�j�  }�j�  jl  suh*j�  hj  hhhh+hNubeh}�(h ]��main-dashboard-admin�ah"]�h$]��main dashboard (admin)�ah&]�h(]�uh*h
hhhhhh+hKhubeh}�(h ]��
deployment�ah"]�h$]��
deployment�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j{  jx  j�  j�  h�h~h�h�j�  j�  j	  j  js  jp  u�	nametypes�}�(j{  Nj�  Nh��h��j�  Nj	  Njs  Nuh }�(jx  hj�  h,h~hxh�h�j�  j�  j  j�  jp  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.